# Bank account kata

Think of your personal bank account experience
When in doubt, go for the simplest solution

### Requirements

* Deposit and Withdrawal  
* Transfer  
* Account statement (date, amount, balance)  
* Statement printing  
* Statement filters (just deposits, withdrawal, date)

### Rules

* One level of indentation per method
* Don’t use the ELSE keyword
* Wrap all primitives and Strings
* First class collections
* One dot per line
* Don’t abbreviate
* Keep all entities small (50 lines)
* No classes with more than two instance variables
* No getters/setters/properties

#### For more information:

-  [Object Calisthenics pdf](http://www.cs.helsinki.fi/u/luontola/tdd-2009/ext/ObjectCalisthenics.pdf)
