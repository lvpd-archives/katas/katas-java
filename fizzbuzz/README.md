# Fizz Buzz Kata

### Feature 1: Basic
* Normal numbers return same number
* Multiples of 3 return Fizz
* Multiples of 5 return Buzz
* Multiples of 3 and 5 return Fizz Buzz

### Feature 2: Replacing digits
* Number containing 3 return Fizz
* Number containing 5 return Buzz

### Feature 3: New value
* Add support for number 7 with Pop 

### Feature 4: Creating variations
* Choose substitution as input of the programm
* Linking substitution together
