package com.groupama.soltec.kata.java8.dataset;

import com.groupama.soltec.kata.java8.entity.OnlineShoppingMall;

import javax.xml.bind.JAXB;
import java.io.File;

public class ClassicOnlineStore {

    protected final OnlineShoppingMall mall = JAXB.unmarshal(new File("src/main/resources/data.xml"), OnlineShoppingMall.class);

}
