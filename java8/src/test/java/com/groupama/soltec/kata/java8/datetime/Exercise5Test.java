package com.groupama.soltec.kata.java8.datetime;

import com.groupama.soltec.kata.java8.annotation.Easy;
import com.groupama.soltec.kata.java8.dataset.DateAndTimes;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Though old Date APIs are not recommended to use with Java8 Date and Time APIs, there could be some cases that you
 * need to do so. Thus you should know how to convert bidirectionally.
 */
public class Exercise5Test {

    @Easy
    @Test
    public void localDateTime2Timestamp() {
        LocalDateTime ldt = DateAndTimes.LDT_20150618_23073050;

        /**
         * Create a {@link Timestamp} from {@link ldt}
         */
        Timestamp timestamp = null;

        assertThat(timestamp.toString()).isEqualTo("2015-06-18 23:07:30.5");
    }

    @Easy
    @Test
    public void localDate2date() {
        LocalDate ld = DateAndTimes.LD_20150618;

        /**
         * Create a {@link Date} from {@link ld}
         */
        Date date = null;

        assertThat(date.toString()).isEqualTo("2015-06-18");
    }

    @Easy
    @Test
    public void timestamp2LocalDateTime() {
        Timestamp timestamp = DateAndTimes.OLD_TIMESTAMP_20150618_23073050;

        /**
         * Create a {@link LocalDateTime} from {@link timestamp}
         */
        LocalDateTime localDateTime = null;

        assertThat(localDateTime.toString()).isEqualTo("2015-06-18T23:07:30.500");
    }

    @Easy
    @Test
    public void date2LocalDate() {
        Date date = DateAndTimes.OLD_DATE_20150618;

        /**
         * Create a {@link LocalDate} from {@link date}
         */
        LocalDate localDate = null;

        assertThat(localDate.toString()).isEqualTo("2015-06-18");
    }
}
