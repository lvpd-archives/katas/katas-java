package com.groupama.soltec.kata.java8.stream;

import com.groupama.soltec.kata.java8.annotation.Easy;
import com.groupama.soltec.kata.java8.dataset.ClassicOnlineStore;
import com.groupama.soltec.kata.java8.entity.Customer;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class Exercise3Test extends ClassicOnlineStore {

    @Easy
    @Test
    public void howManyItemsWanted() {
        List<Customer> customerList = this.mall.getCustomerList();

        /**
         * Count how many items there are in {@link Customer.wantToBuy} using {@link Stream#count}
         */
        long sum = 0L;

        assertThat(sum).isEqualTo(32L);
    }

    @Easy
    @Test
    public void richestCustomer() {
        List<Customer> customerList = this.mall.getCustomerList();

        /**
         * Find the richest customer's budget by using {@link Stream#max} and {@link Comparator#naturalOrder}
         * Don't use {@link Stream#sorted}
         */
        Comparator<Integer> comparator = null;
        Optional<Integer> richestCustomer = null;

        assertThat(comparator.getClass().getSimpleName()).isEqualTo("NaturalOrderComparator");
        assertThat(richestCustomer.get()).isEqualTo(12000);
    }

    @Easy
    @Test
    public void youngestCustomer() {
        List<Customer> customerList = this.mall.getCustomerList();

        /**
         * Find the youngest customer by using {@link Stream#min}
         * Don't use {@link Stream#sorted}
         */
        Comparator<Customer> comparator = null;
        Optional<Customer> youngestCustomer = null;

        assertThat(youngestCustomer.get()).isEqualTo(customerList.get(8));
    }
}
