package com.groupama.soltec.kata.java8.stream;

import com.groupama.soltec.kata.java8.annotation.Difficult;
import com.groupama.soltec.kata.java8.annotation.Easy;
import com.groupama.soltec.kata.java8.dataset.ClassicOnlineStore;
import com.groupama.soltec.kata.java8.entity.Customer;
import com.groupama.soltec.kata.java8.util.CollectorImpl;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import static org.assertj.core.api.Assertions.assertThat;

public class Exercise9Test extends ClassicOnlineStore {

    @Easy
    @Test
    public void simplestStringJoin() {
        List<Customer> customerList = this.mall.getCustomerList();

        /**
         * Implement a {@link Collector} which can create a String with comma separated names shown in the assertion.
         * The collector will be used by serial stream.
         */
        Supplier<Object> supplier = null;
        BiConsumer<Object, String> accumulator = null;
        BinaryOperator<Object> combiner = null;
        Function<Object, String> finisher = null;

        Collector<String, ?, String> toCsv =
            new CollectorImpl<>(supplier, accumulator, combiner, finisher, Collections.emptySet());
        String nameAsCsv = customerList.stream().map(Customer::getName).collect(toCsv);
        assertThat(nameAsCsv).isEqualTo("Joe,Steven,Patrick,Diana,Chris,Kathy,Alice,Andrew,Martin,Amy");
    }

    @Difficult
    @Test
    public void mapKeyedByItems() {
        List<Customer> customerList = this.mall.getCustomerList();

        /**
         * Implement a {@link Collector} which can create a {@link Map} with keys as item and
         * values as {@link Set} of customers who are wanting to buy that item.
         * The collector will be used by parallel stream.
         */
        Supplier<Object> supplier = null;
        BiConsumer<Object, Customer> accumulator = null;
        BinaryOperator<Object> combiner = null;
        Function<Object, Map<String, Set<String>>> finisher = null;

        Collector<Customer, ?, Map<String, Set<String>>> toItemAsKey =
            new CollectorImpl<>(supplier, accumulator, combiner, finisher, EnumSet.of(
                Collector.Characteristics.CONCURRENT,
                Collector.Characteristics.IDENTITY_FINISH));
        Map<String, Set<String>> itemMap = customerList.stream().parallel().collect(toItemAsKey);
        assertThat(itemMap.get("plane")).containsExactlyInAnyOrder("Chris");
        assertThat(itemMap.get("onion")).containsExactlyInAnyOrder("Patrick", "Amy");
        assertThat(itemMap.get("ice cream")).containsExactlyInAnyOrder("Patrick", "Steven");
        assertThat(itemMap.get("earphone")).containsExactlyInAnyOrder("Steven");
        assertThat(itemMap.get("plate")).containsExactlyInAnyOrder("Joe", "Martin");
        assertThat(itemMap.get("fork")).containsExactlyInAnyOrder("Joe", "Martin");
        assertThat(itemMap.get("cable")).containsExactlyInAnyOrder("Diana", "Steven");
        assertThat(itemMap.get("desk")).containsExactlyInAnyOrder("Alice");
    }

    @Difficult
    @Test
    public void bitList2BitString() {
        String bitList = "22-24,9,42-44,11,4,46,14-17,5,2,38-40,33,50,48";

        /**
         * Create a {@link String} of "n"th bit ON.
         * for example
         * "3" will be "001"
         * "1,3,5" will be "10101"
         * "1-3" will be "111"
         * "7,1-3,5" will be "1110101"
         */
        Collector<String, ?, String> toBitString = null;

        String bitString = Arrays.stream(bitList.split(",")).collect(toBitString);
        assertThat(bitString).isEqualTo("01011000101001111000011100000000100001110111010101");
    }
}
