# Minesweeper Kata

The goal of the game is to find all the mines within an MxN field.
To help you, the game shows a number in a square which tells you how many times there are adjacent to that square.
For instance, take the following 4x4 field with 2 mines (which are represented by an <code>*</code> character):
<pre>
*...
....
.*..
....
</pre>
The same filde including the hint numbers described above would look like this:
<pre>
*100
2210
1*10
1110
</pre>

You should write a program that takes input as follows:

The input will consist of an arbitrary number of fields.
The first line of each field contains two integers n and m (0 < n, m <= 100) which stands for the number of lines and columns of the field respectively.
The next n lines contains exactly m characters and represent the field.
Each safe square is represented by a <code>.</code> character and each mine square is represented by an <code>*</code> character.
The first field line where n = m = 0 represents the end of input and should not be processed.

Your program should produce output as follows:

For each field, you must print the following message in a line alone:

Field #x:

Where x stands for the number of the field (starting from 1).
The next n lines should contain the field with the <code>.</code> characters replaced by the number of adjacent mines to that square.
There must be an empty line between field outputs.

Example:
<pre>
4 4
*...
....
.*..
....
3 5
**...
.....
.*...
0 0
</pre>
should output
<pre>
Field #1:
*100
2210
1*10
1110
Field #2:
**100
33200
1*100
</pre>
