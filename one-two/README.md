# The One Two Kata

### First way conversion
We need a method to convert strings to other strings in the following way:
* <code>2</code> &rarr; <code>one two</code>
* <code>1 2</code> &rarr; <code>one one one two</code>
* <code>2 2</code> &rarr; <code>two two</code>
* <code>3 9 9 9 8 8</code> &rarr; <code>one three three nine two height</code>
* <code>1 1 1 1 1 1 1</code> &rarr; <code>seven one</code>
* <code>2 4 4 4 6 6 6 6 6 6</code> &rarr; <code>one two three four five six</code>

To keep things simple, we do not count past 9.
* <code>5 5 5 5 5 5 5 5 5 5 5 5</code> &rarr; <code>nine five three five</code> 

### Do the opposite conversion
