# Palindrome Kata

* Start with short string: 0, 1, 2 then 3 characters
* Longer palindromes
* Handle non-alphabetic characters
* Read palindromes from a file
