package com.groupama.kata.p2o;

public enum MoteurType {
    ESSENCE, DIESEL, ELECTRIQUE
}
