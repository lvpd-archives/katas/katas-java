package com.groupama.kata.p2o.moteur;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MoteurDiesel {
    public boolean prechauffageDemarrage() {
        log.info("Préchauffage");
        log.info("Démarrage");

        return true;
    }
}
