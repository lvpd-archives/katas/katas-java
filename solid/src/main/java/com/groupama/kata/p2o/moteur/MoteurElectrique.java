package com.groupama.kata.p2o.moteur;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MoteurElectrique {
    public boolean simulationBruitDemarrage() {
        log.info("Simulation de bruit");

        return true;
    }
}
