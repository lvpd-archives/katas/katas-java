package com.groupama.kata.p2o.moteur;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MoteurEssence {
    public boolean demarrage() {
        log.info("Demarrage");

        return true;
    }
}
