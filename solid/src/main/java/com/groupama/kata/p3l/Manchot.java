package com.groupama.kata.p3l;

public class Manchot extends Oiseau {

    @Override
    public String getName() {
        return "MANCHOT";
    }

    @Override
    public void setAltitude(int altitude) {
        // Un manchot ne vole pas
    }
}
