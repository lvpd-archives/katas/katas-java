package com.groupama.kata.p4i;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GardienDeZoo implements InteractionOurs {

    @Override
    public boolean nettoyer() {
        log.info("Nettoyage de l'ours");
        return true;
    }

    @Override
    public boolean nourrir() {
        log.info("Alimentation de l'ours");
        return true;
    }

    @Override
    public boolean caresser() {
        throw new RuntimeException("Même pas en rêve");
    }
}
