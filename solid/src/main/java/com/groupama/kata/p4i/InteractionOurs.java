package com.groupama.kata.p4i;

public interface InteractionOurs {
    boolean nettoyer();
    boolean nourrir();
    boolean caresser();
}
