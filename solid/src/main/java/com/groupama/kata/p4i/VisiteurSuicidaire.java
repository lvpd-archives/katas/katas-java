package com.groupama.kata.p4i;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VisiteurSuicidaire implements InteractionOurs {
    @Override
    public boolean nettoyer() {
        log.info("Je ne suis pas là pour laver un ours");
        return false;
    }

    @Override
    public boolean nourrir() {
        log.info("Je ne suis pas là pour nourrir un ours");
        return false;
    }

    @Override
    public boolean caresser() {
        log.info("Allons caresser l'ours");
        return true;
    }
}
