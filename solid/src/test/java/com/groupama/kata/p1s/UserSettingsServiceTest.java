package com.groupama.kata.p1s;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class UserSettingsServiceTest {

    private static final String TEST_EMAIL = "toto@toto.com";

    @Test
    public void shouldAllowUpdateOfUserWithUpdateRights() {
        User user = new User();
        user.setName("shouldAllowUpdateOfUserWithUpdateRights");
        user.setRights(Arrays.asList("INVALID_RIGHT", UserSettingsService.RIGHT_UPDATE_DATA));

        Settings settings = new Settings();
        settings.setEmail(TEST_EMAIL);

        UserSettingsService service = new UserSettingsService();

        assertTrue(service.changeSettings(user, settings));
        assertEquals(TEST_EMAIL, service.getSettings(user).getEmail());
    }

    @Test
    public void shouldNotAllowUpdateOfUserWithNoRight() {
        User user = new User();
        user.setName("shouldNotAllowUpdateOfUserWithNoRight");
        user.setRights(Arrays.asList("INVALID_RIGHT", "INVALID_RIGHT2"));

        Settings settings = new Settings();
        settings.setEmail(TEST_EMAIL);

        UserSettingsService service = new UserSettingsService();

        assertFalse(service.changeSettings(user, settings));
        assertNull(TEST_EMAIL, service.getSettings(user));
    }

    @Test
    public void shouldAllowUpdateOfSuperUser() {
        User user = new User();
        user.setName("shouldAllowUpdateOfUserWithUpdateRights");
        user.setRights(Arrays.asList("INVALID_RIGHT", "SUPER_USER"));

        Settings settings = new Settings();
        settings.setEmail(TEST_EMAIL);

        UserSettingsService service = new UserSettingsService();

        assertTrue(service.changeSettings(user, settings));
        assertEquals(TEST_EMAIL, service.getSettings(user).getEmail());
    }
}
