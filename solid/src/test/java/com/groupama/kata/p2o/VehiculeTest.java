package com.groupama.kata.p2o;

import org.junit.Test;

import static org.junit.Assert.*;

public class VehiculeTest {

    @Test
    public void unVehiculeEssencePeutDemarrer() {
        Vehicule vehicule = new Vehicule(MoteurType.ESSENCE);
        assertTrue(vehicule.start());
    }

    @Test
    public void unVehiculeDieselPeutDemarrer() {
        Vehicule vehicule = new Vehicule(MoteurType.DIESEL);
        assertTrue(vehicule.start());
    }

    @Test
    public void unVehiculeElectriquePeutDemarrer() {
        Vehicule vehicule = new Vehicule(MoteurType.ELECTRIQUE);
        assertTrue(vehicule.start());
    }
}
