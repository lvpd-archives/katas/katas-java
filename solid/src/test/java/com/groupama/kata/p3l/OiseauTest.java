package com.groupama.kata.p3l;

import org.junit.Test;

import static org.junit.Assert.*;

public class OiseauTest {

    private static final int TEST_LATITUDE = 12;
    private static final int TEST_LONGITUDE = 34;
    private static final int TEST_ALTITUDE = 56;

    @Test
    public void shouldUpdatePigeonData() {
        Oiseau oiseau = new Pigeon();
        oiseau.setPosition(TEST_LATITUDE, TEST_LONGITUDE);
        oiseau.setAltitude(TEST_ALTITUDE);

        assertEquals(TEST_LATITUDE, oiseau.getPosition()[0]);
        assertEquals(TEST_LONGITUDE, oiseau.getPosition()[1]);
        assertEquals(TEST_ALTITUDE, oiseau.getAltitude());
    }

    @Test
    public void shouldUpdateAigleData() {
        Oiseau oiseau = new Aigle();
        oiseau.setPosition(TEST_LATITUDE, TEST_LONGITUDE);
        oiseau.setAltitude(TEST_ALTITUDE);

        assertEquals(TEST_LATITUDE, oiseau.getPosition()[0]);
        assertEquals(TEST_LONGITUDE, oiseau.getPosition()[1]);
        assertEquals(TEST_ALTITUDE, oiseau.getAltitude());
    }

    @Test
    public void shouldUpdateManchotData() {
        Oiseau oiseau = new Manchot();
        oiseau.setPosition(TEST_LATITUDE, TEST_LONGITUDE);
        oiseau.setAltitude(TEST_ALTITUDE);

        assertEquals(TEST_LATITUDE, oiseau.getPosition()[0]);
        assertEquals(TEST_LONGITUDE, oiseau.getPosition()[1]);
        assertEquals(TEST_ALTITUDE, oiseau.getAltitude());
    }
}
