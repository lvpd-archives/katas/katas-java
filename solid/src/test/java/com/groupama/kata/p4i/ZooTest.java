package com.groupama.kata.p4i;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ZooTest {

    @Test
    public void shouldWashBear() {
        List<InteractionOurs> acteurs = new ArrayList<>();
        acteurs.add(new GardienDeZoo());
        acteurs.add(new VisiteurSuicidaire());

        acteurs.forEach(a->assertTrue(a.nettoyer()));
    }

    @Test
    public void shouldFeedBear() {
        List<InteractionOurs> acteurs = new ArrayList<>();
        acteurs.add(new GardienDeZoo());
        acteurs.add(new VisiteurSuicidaire());

        acteurs.forEach(a->assertTrue(a.nourrir()));
    }

    @Test
    public void shouldPetBear() {
        List<InteractionOurs> acteurs = new ArrayList<>();
        acteurs.add(new GardienDeZoo());
        acteurs.add(new VisiteurSuicidaire());

        acteurs.forEach(a->assertTrue(a.caresser()));
    }

}
