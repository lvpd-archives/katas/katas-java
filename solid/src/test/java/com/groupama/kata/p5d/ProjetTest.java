package com.groupama.kata.p5d;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProjetTest {

    @Test
    public void shouldIterateOnProject() {
        Projet projet = new Projet();
        int nbIterations = 3;
        for (int i = 0; i < nbIterations; i++) {
            projet.runIteration();
        }

        assertEquals(nbIterations, projet.getIteration());
    }
}
