package com.groupama.soltec.kata.supermarket.model;

public enum ProductUnit {
    Kilo, Each
}
