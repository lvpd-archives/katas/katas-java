package com.groupama.soltec.kata.supermarket.model;

public enum SpecialOfferType {
    ThreeForTwo, TenPercentDiscount, TwoForAmount, FiveForAmount;
}
