package com.groupama.soltec.kata.telldontask.domain;

public enum OrderStatus {
    APPROVED, REJECTED, SHIPPED, CREATED
}
