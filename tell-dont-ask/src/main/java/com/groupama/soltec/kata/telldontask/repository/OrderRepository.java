package com.groupama.soltec.kata.telldontask.repository;

import com.groupama.soltec.kata.telldontask.domain.Order;

public interface OrderRepository {
    void save(Order order);

    Order getById(int orderId);
}
