package com.groupama.soltec.kata.telldontask.repository;

import com.groupama.soltec.kata.telldontask.domain.Product;

public interface ProductCatalog {
    Product getByName(String name);
}
