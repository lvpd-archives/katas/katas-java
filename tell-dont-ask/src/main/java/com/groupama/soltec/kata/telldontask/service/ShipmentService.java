package com.groupama.soltec.kata.telldontask.service;

import com.groupama.soltec.kata.telldontask.domain.Order;

public interface ShipmentService {
    void ship(Order order);
}
