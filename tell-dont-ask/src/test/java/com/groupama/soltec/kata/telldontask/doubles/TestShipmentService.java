package com.groupama.soltec.kata.telldontask.doubles;

import com.groupama.soltec.kata.telldontask.domain.Order;
import com.groupama.soltec.kata.telldontask.service.ShipmentService;

public class TestShipmentService implements ShipmentService {
    private Order shippedOrder = null;

    public Order getShippedOrder() {
        return shippedOrder;
    }

    @Override
    public void ship(Order order) {
        this.shippedOrder = order;
    }
}
