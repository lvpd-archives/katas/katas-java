package com.groupama.soltec.kata.trainreservation.external.bookingreference;

import static spark.Spark.get;
import static spark.Spark.port;

public class BookingReferenceService {
    private static int startingPoint = 123456789;

    public static void main(String[] args) {
        port(8082);
        get("/booking_reference", (req, res) -> {
            res.type("application/json");
            return Integer.toHexString(startingPoint++);
        });
    }
}
