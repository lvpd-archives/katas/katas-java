package com.groupama.soltec.kata.trainreservation.external.traindata;

import com.groupama.soltec.kata.trainreservation.external.traindata.model.TrainData;
import com.groupama.soltec.kata.trainreservation.external.traindata.model.TrainDataSeat;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TrainDataMapper {

    public static Map<String, TrainData> fromJSON(JSONObject trainsDescription) {
        return StreamUtils.asStream(trainsDescription.keys())
                .map(trainId -> new TrainData(trainId, seatsFromJSON(trainsDescription.getJSONObject(trainId))))
                .collect(Collectors.toMap(TrainData::id, Function.identity()));
    }

    private static List<TrainDataSeat> seatsFromJSON(JSONObject seats) {
        JSONObject seatsDescription = seats.getJSONObject("seats");
        return StreamUtils.asStream(seatsDescription.keys())
                .map(seatId -> seatFromJSON(seatId, seatsDescription.getJSONObject(seatId)))
                .collect(Collectors.toList());
    }

    private static TrainDataSeat seatFromJSON(String seatId, JSONObject seatDescription) {
        String seatNumber = seatDescription.getString("seat_number");
        String bookingReference = seatDescription.getString("booking_reference");
        String coach = seatDescription.getString("coach");

        return new TrainDataSeat(seatId, seatNumber, bookingReference, coach);
    }

    public static JSONObject toJSON(TrainData train) {
        return new JSONObject().put(train.id(), seatsToJSON(train.seats()));
    }

    private static JSONObject seatsToJSON(List<TrainDataSeat> seats) {
        JSONObject seatsDescription = new JSONObject();
        seats.forEach(seat -> seatsDescription.put(seat.id(), seatToJSON(seat)));

        return new JSONObject().put("seats", seatsDescription);
    }

    private static JSONObject seatToJSON(TrainDataSeat seat) {
        return new JSONObject()
                .put("seat_number", seat.number())
                .put("booking_reference", seat.bookingReference())
                .put("coach", seat.coach());
    }
}
