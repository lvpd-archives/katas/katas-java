package com.groupama.soltec.kata.trainreservation.external.traindata;

import com.groupama.soltec.kata.trainreservation.external.traindata.exception.SeatNotEmptyException;
import com.groupama.soltec.kata.trainreservation.external.traindata.exception.SeatNotInTrainException;
import com.groupama.soltec.kata.trainreservation.external.traindata.model.TrainData;
import org.json.JSONObject;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static spark.Spark.*;

public class TrainDataService {

    public static final String TRAINDATA_FILE = "traindata/trains.json";
    private static Map<String, TrainData> TRAINS;

    public static void main(String[] args) {
        parseTrains();
        start();
    }

    private static void parseTrains() {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(TrainDataService.TRAINDATA_FILE);

        try (Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name())) {
            JSONObject trainsDescription = new JSONObject(scanner.useDelimiter("\\A").next());
            TRAINS = TrainDataMapper.fromJSON(trainsDescription);
        }
    }

    private static void start() {
        port(8081);
        get("/data_for_train/:train_id", (req, res) -> {
            res.type("application/json");
            return trainDescription(req.params(":train_id"));
        });
        post("/reserve", (req, res) -> {
            JSONObject requestBody = new JSONObject(req.body());
            String trainId = requestBody.getString("train_id");
            String bookingReference = requestBody.getString("booking_reference");

            List<String> seatIds = new ArrayList<>();
            for (int index = 0; index < requestBody.getJSONArray("seats").length(); index++) {
                seatIds.add(requestBody.getJSONArray("seats").getString(index));
            }

            reserve(trainId, seatIds, bookingReference);
            return "OK";
        });
        post("/reset/:train_id", (req, res) -> {
            resetTrainBooking(req.params(":train_id"));
            return "OK";
        });

        exception(SeatNotInTrainException.class, (exception, req, res) -> {
            res.body(exception.format());
            res.status(400);
            res.type("application/json");
        });

        exception(SeatNotEmptyException.class, (exception, req, res) -> {
            res.body(exception.format());
            res.status(400);
            res.type("application/json");
        });
    }

    private static String trainDescription(String trainId) {
        JSONObject trainDescription = TrainDataMapper.toJSON(TRAINS.get(trainId));
        return trainDescription.getJSONObject(trainId).toString();
    }

    private static void reserve(String trainId, List<String> seatIds, String bookingReference) {
        TRAINS.get(trainId).reserve(seatIds, bookingReference);
    }

    private static void resetTrainBooking(String trainId) {
        TRAINS.get(trainId).reset();
    }

}
