package com.groupama.soltec.kata.trainreservation.external.traindata.exception;

public class SeatNotEmptyException extends RuntimeException {
    private final String seatId;

    public SeatNotEmptyException() {
        this.seatId = null;
    }

    public SeatNotEmptyException(String seatId) {
        this.seatId = seatId;
    }

    public String format() {
        return String.format("Already booked with reference %s", seatId);
    }
}
