package com.groupama.soltec.kata.trainreservation.external.traindata.exception;

public class SeatNotInTrainException extends RuntimeException {
    private final String seatId;

    public SeatNotInTrainException() {
        this.seatId = null;
    }

    public SeatNotInTrainException(String seatId) {
        this.seatId = seatId;
    }

    public String format() {
        return String.format("Seat not found %s", seatId);
    }
}
