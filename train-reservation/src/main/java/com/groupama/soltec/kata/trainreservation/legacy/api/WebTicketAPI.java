package com.groupama.soltec.kata.trainreservation.legacy.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.groupama.soltec.kata.trainreservation.legacy.core.WebTicketManager;

import java.io.IOException;

import static spark.Spark.port;
import static spark.Spark.post;

public class WebTicketAPI {

    private final ObjectMapper objectMapper;

    private WebTicketAPI() {
        objectMapper = new ObjectMapper();
    }

    public static void main(String[] args) {
        WebTicketAPI api = new WebTicketAPI();
        api.start();
    }

    private void start() {
        port(8080);
        post("/api/reservation", (req, res) -> {
            RequestDTO request = parseReservationRequest(req.body());
            WebTicketManager webTicketManager = new WebTicketManager();
            res.type("application/json");
            return webTicketManager.reserve(request.train_id, request.number_of_seats);
        });
    }

    private RequestDTO parseReservationRequest(String body) throws IOException {
        return objectMapper.readValue(body, RequestDTO.class);
    }

}
