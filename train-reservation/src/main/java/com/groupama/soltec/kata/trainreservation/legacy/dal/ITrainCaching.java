package com.groupama.soltec.kata.trainreservation.legacy.dal;

import java.util.List;

public interface ITrainCaching {
    void Clear() throws InterruptedException;

    void Save(List<SeatEntity> seatEntities) throws InterruptedException;
}
