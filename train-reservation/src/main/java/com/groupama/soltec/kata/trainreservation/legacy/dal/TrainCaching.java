package com.groupama.soltec.kata.trainreservation.legacy.dal;

import java.util.ArrayList;
import java.util.List;

public class TrainCaching implements ITrainCaching {
    List<SeatEntity> seats = new ArrayList<>();
    @Override
    public void Clear() throws InterruptedException {
        seats.clear();
        Thread.sleep(1000);
    }

    @Override
    public void Save(List<SeatEntity> seatEntities) throws InterruptedException {
        seats.addAll(seatEntities);
        Thread.sleep(1000);
    }
}

final class Factory {
    public static ITrainCaching create() {
        return new TrainCaching();
    }
}
