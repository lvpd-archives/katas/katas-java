package com.groupama.soltec.kata.trainreservation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class TrainReservationTest {

    @Test
    public void test() throws IOException {
        //Given
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target("http://localhost:8080/api/reservation");
        Invocation.Builder request = resource.request();
        request.accept(MediaType.APPLICATION_JSON);
        Entity<String> body = Entity.json("{\"train_id\": \"express_2000\", \"number_of_seats\": 4}");

        //When
        String response = request.post(body, String.class);
        ObjectMapper mapper = new ObjectMapper();
        ReservationDTO reservation = mapper.readValue(response, ReservationDTO.class);

        //Then
        System.out.println(reservation);
        assertThat(reservation.train_id).isEqualTo("express_2000");
        assertThat(reservation.booking_reference).isEqualTo("75bcd15");
        assertThat(reservation.seats).containsExactly("8A", "7A", "8B", "6A");

    }
}
